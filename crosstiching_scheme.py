#!/usr/bin/env python3
import cv2
import numpy as np
from scipy import spatial
from scipy.cluster.vq import vq, kmeans
from scipy.cluster.hierarchy import fclusterdata, linkage
import sys
import os
import getopt
from palette_storage import *
import pystache
import base64
import codecs
import regex as re

CURRENT_SCRIPT_DIR = \
    os.path.dirname(os.path.realpath(__file__))

SYMBOLS_SIZE = 15

PALETTE_DB = \
    os.path.join(
        CURRENT_SCRIPT_DIR,
        "palettes.sqlite")
SYMBOLS_PATH = \
    os.path.join(
        CURRENT_SCRIPT_DIR,
        "Symbols.png")

PALLETE_TEMPLATE_FILE_NAME = \
    os.path.join(
        CURRENT_SCRIPT_DIR,
        "pallete_template.html")


WHITE_IN_LAB_COLORSPACE = [100.0, 0.01, -0.01]


def localmin_mask(values, border_threshold):
    mask_le = values[:-1] <= values[1:]
    mask_ge = values[:-1] >= values[1:]

    mask_local_minima = \
        np.logical_and(
            mask_le[1:],
            mask_ge[:-1])
    mask = \
        np.hstack(
            (mask_le[0:1],
             mask_local_minima,
             mask_ge[-1:]))

    if border_threshold != None:
        mask[mask] = values[mask] < border_threshold

    return mask


def is255_mask(values, border_threshold):
    return values == 255


def is0_mask(values, border_threshold):
    return values == 0


def localmax_mask(values, border_threshold):
    mask_le = values[:-1] <= values[1:]
    mask_ge = values[:-1] >= values[1:]

    mask_local_minima = \
        np.logical_and(
            mask_ge[1:],
            mask_le[:-1])

    mask = \
        np.hstack(
            (mask_ge[0:1],
             mask_local_minima,
             mask_le[-1:]))

    if(border_threshold != None):
        mask[mask] = values[mask] > border_threshold

    return mask


def get_average_colors(image, axis):
    return np.rint(np.mean(image, axis))\
             .astype(np.uint8)


def get_hsv(arr_bgr):
    img_bgr = arr_bgr[:, np.newaxis, :]
    hsv = cv2.cvtColor(img_bgr, cv2.COLOR_BGR2HSV)
    return hsv[:, 0, :]


def plot_rows(path, arr_bgr, n_col):
    cv2.imwrite(
        path,
        np.repeat(
            arr_bgr[:, np.newaxis, :],
            n_col,
            1))


def plot_cols(path, arr_bgr, n_row):
    cv2.imwrite(
        path,
        np.repeat(
            arr_bgr[np.newaxis, :, :],
            n_row,
            0))


def plot_grid(path, image, row_border_mask, col_border_mask, color):
    imcopy = np.copy(image)

    color_ar = np.array(color)

    imcopy[row_border_mask, :, :] = color_ar
    imcopy[:, col_border_mask, :] = color_ar

    cv2.imwrite(path, imcopy)


def get_cell_borders(grid_row_or_col_mask):
    """Finds indexes of cell boundaries.
    Cells are contigous regions filled with zeros in sequence of 0 and 1.


    Arguments:
        grid_row_or_col_mask {list of ones and zeros} -- [description]

    Returns:
        list of tuples -- cell borders
    """
    cell_borders = []

    CELL = 1
    BORDER = 2
    CELL_CANDIDATE = 3

    state = CELL
    start_idx = 0
    start_idx2 = 0

    for i in range(0, grid_row_or_col_mask.shape[0]):
        if(state == CELL):
            if(grid_row_or_col_mask[i] == 1):
                state = BORDER
                if(i-start_idx > 1):
                    cell_borders.append([start_idx, i-1])
                start_idx = i
        elif(state == BORDER):
            if(grid_row_or_col_mask[i] == 0):
                state = CELL_CANDIDATE
                start_idx2 = i
        elif(state == CELL_CANDIDATE):
            if(grid_row_or_col_mask[i] == 0):
                state = CELL
                start_idx = start_idx2
            else:
                state = BORDER
    return cell_borders


def get_1px_per_cross_image(data, row_borders, col_borders):
    """Extracts 1 px per cross image from original image

    Arguments:
        data {(w,h,3) u8 array} -- original image
        row_borders {list of int tuples} -- row border indexes (see get_cell_borders(...))
        col_borders {list of int tuples} -- column border indexes (see get_cell_borders(...))

    Returns:
        (cells_w, cells_h, 3) u8 array -- output image
    """
    out_data = np.empty(
        (len(row_borders), len(col_borders), 3), dtype=np.uint8)
    for idx_col, col in enumerate(col_borders):
        for idx_row, row in enumerate(row_borders):
            tmpdat = data[row[0]:row[1]+1, col[0]:col[1]+1, :]
            size = tmpdat.shape[0]*tmpdat.shape[1]

            mean_color = np.mean(tmpdat, (0, 1))
            out_data[idx_row, idx_col, :] = mean_color
            data[row[0]:row[1]+1, col[0]:col[1]+1, :] = mean_color

    return out_data


def border_criterion_ave_value(image, idx):
    average_bgr = get_average_colors(image, idx)
    # plot_rows(\
    #    'row_average_colors.png',
    #    average_bgr,
    #    image.shape[1])
    hsv = get_hsv(average_bgr)
    return hsv[:, 2]


def border_criterion_max_red(image, idx):
    max_color = \
        np.min(
            np.linalg.norm(
                image-np.array([0.0, 0.0, 255.0]),
                ord=np.inf,
                axis=2),
            axis=idx)

    return np.rint(max_color).astype(np.uint8)


def border_criterion_max_green(image, idx):
    max_color = \
        np.min(
            np.linalg.norm(
                image-np.array([0.0, 255.0, 0.0]),
                ord=np.inf,
                axis=2),
            axis=idx)
    return np.rint(max_color).astype(np.uint8)


def border_criterion_max_blue(image, idx):
    max_color = \
        np.min(
            np.linalg.norm(
                image-np.array([255.0, 0.0, 0.0]),
                ord=np.inf,
                axis=2),
            axis=idx)
    return np.rint(max_color).astype(np.uint8)


def border_criterion_var_value(image, idx):
    hsv_img = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

    #criterion = np.mean(np.abs(np.gradient(hsv_img[:,:,0], axis=idx)), axis=idx)

    criterion = \
        np.linalg.norm(
            np.stack((
                np.median(
                    np.abs(np.gradient(hsv_img[:, :, 0], axis=1)), axis=idx),
                np.median(
                    np.abs(np.gradient(hsv_img[:, :, 1], axis=1)), axis=idx),
                np.median(np.abs(np.gradient(hsv_img[:, :, 2], axis=1)), axis=idx))),
            ord=2,
            axis=0)

    #criterion = np.var(hsv_img[:,:,0], idx)

    # criterion = \
    #    np.max(\
    #        np.stack((\
    #            np.var(hsv_img[:,:,0], idx),
    #            np.var(hsv_img[:,:,1], idx),
    #            np.var(hsv_img[:,:,2], idx))),
    #        0)

    # criterion = \
    #    np.linalg.norm(\
    #        np.stack((\
    #            np.var(hsv_img[:,:,0], idx),
    #            np.var(hsv_img[:,:,1], idx),
    #            np.var(hsv_img[:,:,2], idx))),
    #        ord=2,
    #        axis=0)

    return np.rint(criterion).astype(np.uint8)


def get_border_detector(border_detector_name):
    if(border_detector_name == "ave-min"):
        return (border_criterion_ave_value, localmin_mask)
    elif(border_detector_name == "ave-max"):
        return (border_criterion_ave_value, localmax_mask)
    elif(border_detector_name == "var-min"):
        return (border_criterion_var_value, localmin_mask)
    elif(border_detector_name == "var-max"):
        return (border_criterion_var_value, localmax_mask)
    elif(border_detector_name == "max-red"):
        return (border_criterion_max_red, is0_mask)
    elif(border_detector_name == "max-green"):
        return (border_criterion_max_green, is0_mask)
    elif(border_detector_name == "max-blue"):
        return (border_criterion_max_blue, is0_mask)
    else:
        raise LookupError("Unknown border detector type '%s'" %
                          border_detector_name)


def find_h_borders_mask(image, border_detector_name, border_threshold):
    border_criterion, border_detector = get_border_detector(
        border_detector_name)

    row_criterion = border_criterion(image, 1)
    # print("row_criterion")
    # print(row_criterion)
    row_border_mask = border_detector(row_criterion, border_threshold)
    return row_border_mask


def find_v_borders_mask(image, border_detector_name, border_threshold):
    border_criterion, border_detector = get_border_detector(
        border_detector_name)

    col_criterion = border_criterion(image, 0)
    # print("col_criterion")
    # print(col_criterion[:30])
    col_border_mask = border_detector(col_criterion, border_threshold)
    return col_border_mask


def load_mask(length, boundaries_fpath):
    idx_true = np.loadtxt(boundaries_fpath, dtype=np.int64, delimiter=',')
    idx_true = np.extract(idx_true < length, idx_true)
    mask = np.zeros(length, dtype=np.bool)
    mask[idx_true] = 1
    return mask


def save_mask(mask, boundaries_fpath):
    idx_true = np.argwhere(mask)
    np.savetxt(boundaries_fpath, np.reshape(
        idx_true, (1, idx_true.shape[0])), delimiter=',', fmt='%d')


def find_h_borders(
        input_file,
        output_file,
        border_detector_name,
        border_threshold,
        debug_output_image_path=None):

    image = cv2.imread(input_file)

    row_border_mask = \
        find_h_borders_mask(
            image,
            border_detector_name,
            border_threshold)

    if debug_output_image_path:
        plot_grid(
            debug_output_image_path,
            image,
            row_border_mask,
            np.zeros(image.shape[1], dtype=np.bool),
            [0, 0, 255])

    save_mask(row_border_mask, output_file)


def find_v_borders(
        input_file,
        output_file,
        border_detector_name,
        border_threshold,
        debug_output_image_path=None):

    image = cv2.imread(input_file)

    col_border_mask = \
        find_v_borders_mask(
            image,
            border_detector_name,
            border_threshold)

    if debug_output_image_path:
        plot_grid(
            debug_output_image_path,
            image,
            np.zeros(image.shape[0], dtype=np.bool),
            col_border_mask,
            [0, 0, 255])

    save_mask(col_border_mask, output_file)


def extract_image(
        input_file,
        output_file,
        border_detector_name,
        border_threshold,
        h_boundaries_fpath=None,
        v_boundaries_fpath=None):

    image = cv2.imread(input_file)
    # print image.shape

    row_border_mask = \
        find_h_borders_mask(
            image,
            border_detector_name,
            border_threshold) \
        if h_boundaries_fpath == None else \
        load_mask(image.shape[0], h_boundaries_fpath)

    col_border_mask = \
        find_h_borders_mask(
            image,
            border_detector_name,
            border_threshold) \
        if v_boundaries_fpath == None else \
        load_mask(image.shape[1], v_boundaries_fpath)

    plot_grid(
        'grid.png',
        image,
        row_border_mask,
        col_border_mask,
        [0, 0, 255])

    row_borders = get_cell_borders(row_border_mask)
    col_borders = get_cell_borders(col_border_mask)

    out_data = \
        get_1px_per_cross_image(
            image,
            row_borders,
            col_borders)

    cv2.imwrite(output_file, out_data)


def convert_to_pallete(
    input_file, 
    output_file, 
    manufacturer_name, 
    max_colors_number=40,
    preferred_symbol_indexes=None):


    # http://stackoverflow.com/a/2636477
    pallete_storage = MoulinePaletteManager(PALETTE_DB)

    palette = Palette(pallete_storage, manufacturer_name)    


    image = cv2.imread(input_file)

    arr_clab = \
        cv2.cvtColor(image, cv2.COLOR_BGR2LAB)\
           .reshape(image.shape[0]*image.shape[1], 3)\
           .astype(np.float)

    distances, color_indexes = palette.query(arr_clab)

    print(("%s unique colors after mapping to palette" %
           (np.unique(color_indexes).shape[0])))
    print(("max color distances range %s ... %s" %
           (np.min(distances), np.max(distances))))

    image_bgr_converted_to_palette =\
        palette\
            .get_bgr(color_indexes)\
            .reshape(image.shape[0], image.shape[1], 3)

    cv2.imwrite(output_file, image_bgr_converted_to_palette)
    
    has_prefered_symbols = bool(preferred_symbol_indexes)

    palette.save_scheme(
        color_indexes.reshape(image.shape[0], image.shape[1]),
        SymbolsSet(
            SYMBOLS_PATH,
            SYMBOLS_SIZE,
            exclude_equal_density_symbols=(not has_prefered_symbols),
            preferred_symbol_indexes=preferred_symbol_indexes),
        output_file+"_scheme.png",
        output_file+"_palette.htm")


class SymbolsSet:
    def __init__(
            self,
            input_file,
            symbol_size,
            exclude_equal_density_symbols=False,
            preferred_symbol_indexes=None):

        image_bgr = cv2.imread(input_file)
        self.__load_symbols(image_bgr, symbol_size)

        if(preferred_symbol_indexes):
            n_symbols = len(self._symbols)

            if any([idx > n_symbols for idx in preferred_symbol_indexes]):
                raise ValueError(
                    ("Some of preferred symbol indexes are greater than number of symbols (%d)") % (
                        n_symbols))

            self._symbols = [self._symbols[i-1]
                             for i in preferred_symbol_indexes]

        n_non_white_pixels_count_arr = [np.count_nonzero(
            np.logical_not(np.all(x == 255, 2))) for x in self._symbols]

        if(exclude_equal_density_symbols):
            n_non_white_pixels_count_arr, unique_idx = \
                np.unique(
                    n_non_white_pixels_count_arr,
                    return_index=True)
            self._symbols = [self._symbols[i] for i in unique_idx]

        self._symbols = [self._symbols[i]
                         for i in np.argsort(n_non_white_pixels_count_arr)]
        print("Symbol pixel counts")
        print((np.sort(n_non_white_pixels_count_arr)))

    def __load_symbols(self, image_bgr, symbol_size):
        n_symbols_in_row, remainder = divmod(
            image_bgr.shape[0]-1, symbol_size+1)
        if(remainder != 0):
            raise ValueError("Noninteger number of symbols in row")

        n_symbols_in_column, remainder = divmod(
            image_bgr.shape[1]-1, symbol_size+1)
        if(remainder != 0):
            raise ValueError("Noninteger number of symbols in coloumn")

        self._symbol_size = symbol_size

        col_borders = [i*(symbol_size+1) for i in range(1, n_symbols_in_row+1)]
        row_borders = [i*(symbol_size+1)
                       for i in range(1, n_symbols_in_column+1)]

        rows = [row[1:, :] for row in np.split(image_bgr, row_borders, 0)[:-1]]

        self._symbols = []
        for row in rows:
            row_of_symbols = [col[:, 1:]
                              for col in np.split(row, col_borders, 1)[:-1]]
            self._symbols.extend(
                [x for x in row_of_symbols if(not np.all(x == 255))])

    @property
    def n_symbols(self):
        return len(self._symbols)

    @property
    def symbol_size(self):
        return self._symbol_size

    def __getitem__(self, key):
        return self._symbols[key]


class Palette:
    def __init__(self, palette_storage, manufacturer_name):
        with codecs.open(PALLETE_TEMPLATE_FILE_NAME, 'r', encoding='utf8') as f:
            self._palette_html_template = \
                pystache.parse(f.read())

        self._pallete_storage = palette_storage
        self._manufacturer_name = manufacturer_name
        codes, palette_bgrlab =\
            self._pallete_storage\
                .get_pallete(manufacturer_name)

        self._codes = codes
        self._palette_bgrlab = palette_bgrlab

        self._palette_lab_tree =\
            spatial.KDTree(
                palette_bgrlab[:, 3:]
                .astype(np.float))

    def query(self, arr_clab):
        return self._palette_lab_tree\
                   .query(arr_clab, p=2)

    def get_bgr(self, idexes_arr):
        return self._palette_bgrlab[idexes_arr, :3]

    def get_lab(self, idexes_arr):
        return self._palette_bgrlab[idexes_arr, 3:]

    def get_hsv(self, idexes_arr):
        arr_bgr = self._palette_bgrlab[idexes_arr, :3]
        img_bgr = arr_bgr[:, np.newaxis, :]
        hsv = cv2.cvtColor(img_bgr, cv2.COLOR_BGR2HSV)
        return hsv[:, 0, :]

    def get_codes(self, idexes_arr):
        return [self._codes[i] for i in idexes_arr]

    def save_scheme(
            self,
            indexes_2d_arr,
            symbols_set,
            output_file_scheme,
            output_file_codes,
            font_size=15):

        unique_color_idxarr, unique_color_pixel_counts = \
            np.unique(indexes_2d_arr,  return_counts=True)

        if(unique_color_idxarr.shape[0] > symbols_set.n_symbols):
            raise ValueError(
                ("Image contains more unique colors (%d) " +
                 "than number of symbols in collection (%d)") % (
                    unique_color_idxarr.shape[0],
                    symbols_set.n_symbols))

        unique_colors_hsv = self.get_hsv(unique_color_idxarr)

        idx_sorted_value = np.argsort(unique_colors_hsv[:, 2])[::-1]

        unique_color_sorted_idxarr = unique_color_idxarr[idx_sorted_value]
        unique_color_sorted_pixel_counts = unique_color_pixel_counts[idx_sorted_value]

        n_row = indexes_2d_arr.shape[0]
        n_col = indexes_2d_arr.shape[1]
        img_arr = \
            np.empty(
                (n_row*(symbols_set.symbol_size+3)+3,
                 n_col*(symbols_set.symbol_size+3)+3,
                 3),
                dtype=np.uint8)

        img_arr[:, :, :] = 255

        img_arr[1::symbols_set.symbol_size+3, :, :] = 0
        img_arr[0::(symbols_set.symbol_size+3)*10, :, :] = 0
        img_arr[2::(symbols_set.symbol_size+3)*10, :, :] = 0

        img_arr[:, 1::symbols_set.symbol_size+3, :] = 0
        img_arr[:, 0::(symbols_set.symbol_size+3)*10, :] = 0
        img_arr[:, 2::(symbols_set.symbol_size+3)*10, :] = 0

        for symbol_idx, color_idx in enumerate(unique_color_sorted_idxarr):
            symbol = symbols_set[symbol_idx]
            for cell_idx in np.argwhere(indexes_2d_arr == color_idx):
                img_arr[
                    cell_idx[0]*(symbols_set.symbol_size+3)+3:
                    cell_idx[0]*(symbols_set.symbol_size+3) +
                    3+symbols_set.symbol_size,
                    cell_idx[1]*(symbols_set.symbol_size+3)+3:
                    cell_idx[1]*(symbols_set.symbol_size+3) +
                    3+symbols_set.symbol_size,
                    :] = symbol

        cv2.imwrite(output_file_scheme, img_arr)

        self._write_legend_html(
            symbols_set,
            unique_color_sorted_idxarr,
            unique_color_sorted_pixel_counts,
            output_file_codes)

    def _write_legend_html(
            self,
            symbols_set,
            unique_color_sorted_idxarr,
            unique_color_sorted_pixel_counts,
            legend_file_name):

        palette_colors_bgr = self.get_bgr(unique_color_sorted_idxarr)
        palette_symbols = \
            [symbols_set[i]
             for i in range(unique_color_sorted_idxarr.shape[0])]
        palette_codes = self.get_codes(unique_color_sorted_idxarr)

        palette_data = []
        for i in range(unique_color_sorted_idxarr.shape[0]):
            b, symbol_byte_arr = cv2.imencode('.png', palette_symbols[i])
            symbol_base64_str = base64.b64encode(symbol_byte_arr)

            color_sample_bgr = np.empty((20, 40, 3), dtype=np.uint8)
            color_sample_bgr[:, :] = palette_colors_bgr[i]
            b, color_sample_byte_arr = cv2.imencode('.png', color_sample_bgr)
            color_sample_base64_str = base64.b64encode(color_sample_byte_arr)

            color_bgr = palette_colors_bgr[i, :]
            color_data = {
                "code": palette_codes[i],
                "rgb_hex_string": ''.join(
                    '{:02X}'.format(x) for x in
                    [color_bgr[2], color_bgr[1], color_bgr[0]]),
                "color_sample_png_data": color_sample_base64_str,
                "npixels": unique_color_sorted_pixel_counts[i],
                "symbol_png_data": symbol_base64_str}
            palette_data.append(color_data)

        with codecs.open(legend_file_name, 'w', encoding='utf8') as f:
            renderer = pystache.Renderer()
            f.write(
                renderer.render(
                    self._palette_html_template,
                    {'palette': palette_data,
                     'manufacturer': self._manufacturer_name}))


def convert_to_pallete_agglomeration(
        input_file,
        output_file,
        manufacturer_name,
        threshold=2.0,
        preferred_symbol_indexes=None):

    pallete_storage = MoulinePaletteManager(PALETTE_DB)
    palette = Palette(pallete_storage, manufacturer_name)

    image = cv2.imread(input_file)

    arr_clab = convert_to_clab(image)

    # non white pixels mask
    mask_non_background = \
        np.sum(
            image.reshape(image.shape[0]*image.shape[1], 3) < 255,
            1) == 3

    non_background_pixels = arr_clab[mask_non_background, :]
    print(("%s of %s pixels are not background" %
           (non_background_pixels.shape[0], arr_clab.shape[0])))
    has_background_pixels = \
        non_background_pixels.shape[0] < arr_clab.shape[0]

    # only apply clustering to non-background pixels
    cluster_idx_arr =\
        fclusterdata(
            non_background_pixels,
            threshold,
            criterion='inconsistent')-1

    cluster_numbers = np.unique(cluster_idx_arr)

    print("number of clusters = %s" % (cluster_numbers.shape[0]+1))

    cluster_mean_colors_clab = np.empty((cluster_numbers.shape[0], 3))
    for cluster_number in cluster_numbers:
        cluster_mean_colors_clab[cluster_number, :] = \
            np.mean(
                non_background_pixels[cluster_idx_arr == cluster_number, :],
                0)

    dist, cluster_palette_idx_arr = \
        palette.query(cluster_mean_colors_clab)

    print(("number of colors = %s" %
           np.unique(cluster_palette_idx_arr).shape[0]))

    posterized_img_palette_idx_arr =\
        cluster_palette_idx_arr[cluster_idx_arr]

    posterized_img_bgr_flat = np.empty(arr_clab.shape, dtype=np.uint8)
    posterized_img_bgr_flat[:, :] = 255  # white background
    posterized_non_background_pixels_bgr = palette.get_bgr(
        posterized_img_palette_idx_arr)
    posterized_img_bgr_flat[mask_non_background,
                            :] = posterized_non_background_pixels_bgr

    posterized_img_bgr =\
        posterized_img_bgr_flat\
        .reshape(image.shape[0], image.shape[1], 3)

    cv2.imwrite(output_file, posterized_img_bgr)

    pixel_paletete_color_indexes =\
        np.empty(
            (image.shape[0]*image.shape[1]),
            dtype=np.uint8)

    if(has_background_pixels):
        mask_background = \
            np.logical_not(mask_non_background)
        white_color_index = \
            palette.query([WHITE_IN_LAB_COLORSPACE])[0]
        pixel_paletete_color_indexes[mask_background] = \
            white_color_index

    pixel_paletete_color_indexes[mask_non_background] = \
        posterized_img_palette_idx_arr

    has_prefered_symbols = bool(preferred_symbol_indexes)

    palette.save_scheme(
        pixel_paletete_color_indexes
        .reshape((image.shape[0], image.shape[1])),
        SymbolsSet(
            SYMBOLS_PATH,
            SYMBOLS_SIZE,
            exclude_equal_density_symbols=(not has_prefered_symbols),
            preferred_symbol_indexes=preferred_symbol_indexes),
        output_file+"_scheme.png",
        output_file+"_palette.htm")


def convert_to_clab(image):
    arr_clab = \
        cv2.cvtColor(image, cv2.COLOR_BGR2LAB)\
           .reshape(image.shape[0]*image.shape[1], 3)\
           .astype(np.float)
    return arr_clab


def convert_to_pallete_kmeans(
        input_file,
        output_file,
        manufacturer_name,
        max_colors_number=40,
        preferred_symbol_indexes=None):

    # http://stackoverflow.com/a/2636477
    pallete_storage = MoulinePaletteManager(PALETTE_DB)
    palette = Palette(pallete_storage, manufacturer_name)

    image = cv2.imread(input_file)

    arr_clab = convert_to_clab(image)

    distances, color_indexes = palette.query(arr_clab)

    print(("%s unique colors after mapping to palette" %
           (np.unique(color_indexes).shape[0])))
    print(("max color distances range %s ... %s" %
           (np.min(distances), np.max(distances))))

    # calculating LAB color space cluster centroids
    # (n colors max)
    centroid_colors_lab, distortions = \
        kmeans(
            arr_clab,
            max_colors_number)

    distances2, closest_to_centroids_palette_colors_idx =\
        palette.query(centroid_colors_lab)

    print(("%s unique colors after posterization" %
           (np.unique(closest_to_centroids_palette_colors_idx).shape[0])))
    print(("max color distances range %s ... %s" %
           (np.min(distances2), np.max(distances2))))

    # Converting LAB cluster centroid colors to RGB
    centroid_palette_colors_rgb =\
        palette.get_bgr(closest_to_centroids_palette_colors_idx)

    # Assigning nearest color centroid index to each pixel
    centroid_idx_arr, distortions2 =\
        vq(arr_clab,
           centroid_colors_lab)

    # Converting centroid index array to posterized image
    posterized_img_bgr =\
        centroid_palette_colors_rgb[centroid_idx_arr, :]\
        .reshape(image.shape[0], image.shape[1], 3)

    cv2.imwrite(output_file, posterized_img_bgr)

    pixel_paletete_color_indexes = \
        closest_to_centroids_palette_colors_idx[centroid_idx_arr]\
        .reshape(image.shape[0], image.shape[1])

    has_prefered_symbols = bool(preferred_symbol_indexes)

    palette.save_scheme(
        pixel_paletete_color_indexes,
        SymbolsSet(
            SYMBOLS_PATH,
            SYMBOLS_SIZE,
            exclude_equal_density_symbols=(not has_prefered_symbols),
            preferred_symbol_indexes=preferred_symbol_indexes),
        output_file+"_scheme.png",
        output_file+"_palette.htm")


def print_usage():
    print(
        """usage:
       extract.py -i|--input-file <path> -o|--output-file <path> -a|--action <action>
       
       -i|--input-file   input file path
       -o|--output-file output file path
       -a|--action action available actions       
         extract
         find-v-borders
         find-h-borders
         palette-DMC
         palette-Gama
         palette-kmeans-DMC
         palette-kmeans-Gama
         palette-agglomeration-DMC
         palette-agglomeration-Gama
       -n|--ncolors max number of colors in palette
       --h-boundaries file with coma separated horizontal boundaries 
       --v-boundaries file with coma separated vertical boundaries
       -t|--lab-threshold La*b* threshold parameter for clustering""")


def _check_input_file(input_file):
    if(input_file == None):
        print("ERROR: Input file not specified")
        sys.exit(2)
    if not os.path.isfile(input_file):
        print("ERROR: input file '%s' not found" % input_file)
        sys.exit(3)


def _check_output_file(output_file):
    if(output_file == None):
        print("ERROR: Output file not specified")
        sys.exit(2)


def get_parameters():
    input_file = None
    output_file = None
    action = None
    ncolors = 15
    threshold = 2.0
    border_threshold = None
    border_detector = "ave-min"
    h_boundaries_fpath = None
    v_boundaries_fpath = None
    preferred_symbol_indexes = None
    debug_output_image_path = None

    try:
        opts, args = \
            getopt.getopt(
                sys.argv[1:],
                "ha:i:o:n:t:b:",
                ["help=",
                 "input-file=",
                 "output-file=",
                 "action=",
                 "ncolors=",
                 "lab-threshold=",
                 "border-detector=",
                 "h-boundaries=",
                 "v-boundaries=",
                 "border-threshold=",
                 "preferred-symbols=",
                 "debug-output-img="])
    except getopt.GetoptError:
        print("ERROR: Invalid parameters")
        print_usage()
        sys.exit(2)

    SYMBOLS_IDX_LIST_REXP = re.compile(r'\s*(\d+)(?:\s*,\s*(\d+))+')

    for opt, arg in opts:
        if opt in ('-h', '--help'):
            print_usage()
            sys.exit()
        elif opt in ('-i', '--input-file'):
            input_file = arg
        elif opt in ('-o', '--output-file'):
            output_file = arg
        elif opt in ('-a', '--action'):
            action = arg
        elif opt in ('-n', '--ncolors'):
            ncolors = int(arg)
        elif opt in ('-t', '--lab-threshold'):
            threshold = float(arg)
        elif opt in ('-b', '--border-detector'):
            border_detector = arg
        elif opt == '--h-boundaries':
            h_boundaries_fpath = arg
        elif opt == '--v-boundaries':
            v_boundaries_fpath = arg
        elif opt == '--border-threshold':
            border_threshold = int(arg)
        elif opt == '--debug-output-img':
            debug_output_image_path = arg
        elif opt == '--preferred-symbols':
            m = SYMBOLS_IDX_LIST_REXP.match(arg)
            if not m:
                print("'%s' is not a valid symbol indexes list" % arg)
                sys.exit(2)
            preferred_symbol_indexes = \
                [int(idx_str) for idx_str in (m.captures(1) + m.captures(2))]

    if(action == None):
        print("ERROR: Action not specified")
        sys.exit(2)

    return (
        action,
        input_file,
        output_file,
        ncolors,
        threshold,
        border_detector,
        border_threshold,
        h_boundaries_fpath,
        v_boundaries_fpath,
        preferred_symbol_indexes,
        debug_output_image_path)


if __name__ == "__main__":

    action,\
        input_file,\
        output_file,\
        ncolors,\
        threshold,\
        border_detector,\
        border_threshold,\
        h_boundaries_fpath,\
        v_boundaries_fpath,\
        preferred_symbol_indexes,\
        debug_output_image_path = get_parameters()

    _check_input_file(input_file)
    _check_output_file(output_file)

    if action == 'extract':
        print(("Extracting schema from '%s' to '%s' using '%s' border detector" % (
            input_file, output_file, border_detector)))
        extract_image(input_file, output_file, border_detector,
                      border_threshold, h_boundaries_fpath, v_boundaries_fpath)
    elif action == 'find-v-borders':
        print(("Searching vertical cell boundaries in '%s' using '%s' border detector with threshold=%s. Out file path = '%s'" % (
            input_file, border_detector, border_threshold, output_file)))

        find_v_borders(input_file, output_file,
                       border_detector, border_threshold,
                       debug_output_image_path=debug_output_image_path)
    elif(action == 'find-h-borders'):
        print(("Searching horizontal cell boundaries in '%s' using '%s' border detector with threshold=%s. Out file path = '%s'" % (
            input_file, border_detector, border_threshold, output_file)))
        find_h_borders(input_file, output_file,
                       border_detector, border_threshold,
                       debug_output_image_path=debug_output_image_path)
    elif(action == 'palette-DMC'):
        # https://docs.scipy.org/doc/scipy-0.14.0/reference/generated/scipy.spatial.KDTree.html
        print(("Converting '%s' to DMC palette -> '%s'" %
               (input_file, output_file)))
        sys.setrecursionlimit(10000)
        convert_to_pallete(
            input_file,
            output_file,
            'DMC',
            max_colors_number=ncolors,
            preferred_symbol_indexes=preferred_symbol_indexes)
    elif(action == 'palette-Gamma'):
        # https://docs.scipy.org/doc/scipy-0.14.0/reference/generated/scipy.spatial.KDTree.html
        print(("Converting '%s' to Gamma palette -> '%s'" %
               (input_file, output_file)))
        sys.setrecursionlimit(10000)
        convert_to_pallete(
            input_file,
            output_file,
            'Gamma',
            max_colors_number=ncolors,
            preferred_symbol_indexes=preferred_symbol_indexes)
    elif(action == 'palette-kmeans-DMC'):
        # https://docs.scipy.org/doc/scipy-0.14.0/reference/generated/scipy.spatial.KDTree.html
        sys.setrecursionlimit(10000)
        print(("Converting '%s' to DMC palette with k-means color clustering (max %s colors) -> '%s'" %
               (input_file, ncolors, output_file)))
        convert_to_pallete_kmeans(
            input_file,
            output_file,
            'DMC',
            max_colors_number=ncolors,
            preferred_symbol_indexes=preferred_symbol_indexes)
    elif(action == 'palette-kmeans-Gamma'):
        # https://docs.scipy.org/doc/scipy-0.14.0/reference/generated/scipy.spatial.KDTree.html
        sys.setrecursionlimit(10000)
        print(("Converting '%s' to Gamma palette with k-means color clustering (max %s colors) -> '%s'" %
               (input_file, ncolors, output_file)))
        convert_to_pallete_kmeans(
            input_file,
            output_file,
            'Gamma',
            max_colors_number=ncolors,
            preferred_symbol_indexes=preferred_symbol_indexes)
    elif(action == 'palette-agglomeration-DMC'):
        # https://docs.scipy.org/doc/scipy-0.14.0/reference/generated/scipy.spatial.KDTree.html
        sys.setrecursionlimit(10000)
        print(("Converting '%s' to DMC palette with agglomeration color clustering (threshold=%s ) -> '%s'" %
               (input_file, threshold, output_file)))
        convert_to_pallete_agglomeration(
            input_file,
            output_file,
            'DMC',
            threshold=threshold,
            preferred_symbol_indexes=preferred_symbol_indexes)
    elif(action == 'palette-agglomeration-Gamma'):
        # https://docs.scipy.org/doc/scipy-0.14.0/reference/generated/scipy.spatial.KDTree.html
        sys.setrecursionlimit(10000)
        print(("Converting '%s' to Gamma palette with agglomeration color clustering (threshold=%s ) -> '%s'" %
               (input_file, threshold, output_file)))
        convert_to_pallete_agglomeration(
            input_file,
            output_file,
            'Gamma',
            threshold=threshold,
            preferred_symbol_indexes=preferred_symbol_indexes)
    else:
        print(("ERROR: unsupported action '%s'" % action))
