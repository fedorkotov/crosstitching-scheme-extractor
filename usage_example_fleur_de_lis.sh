#!/bin/bash

# Extracting row and column borders
echo ======================
./crosstiching_scheme.py \
    --action find-h-borders \
    --input-file examples/Fleur-de-lis_aligned.jpg \
    --border-detector ave-min \
    --border-threshold 110 \
    --output-file examples/Fleur-de-lis_aligned_h.csv \
    --debug-output-img examples/Fleur-de-lis_aligned_h.png

echo ======================
./crosstiching_scheme.py \
    --action find-v-borders \
    --input-file examples/Fleur-de-lis_aligned.jpg \
    --border-detector ave-min \
    --border-threshold 100 \
    --output-file examples/Fleur-de-lis_aligned_v.csv \
    --debug-output-img examples/Fleur-de-lis_aligned_v.png

# Converting one pixel per cross image to mouline thread colors palette
echo ======================
./crosstiching_scheme.py \
    --action extract \
    --input-file examples/Fleur-de-lis_aligned.jpg \
    --h-boundaries examples/Fleur-de-lis_aligned_h.csv \
    --v-boundaries examples/Fleur-de-lis_aligned_v.csv \
    --output-file examples/Fleur-de-lis_aligned_1pxcross.png    


# Converting one pixel per cross image to mouline thread colors palette
echo ======================
./crosstiching_scheme.py \
    --action palette-kmeans-DMC \
    --ncolors 2 \
    --input-file examples/Fleur-de-lis_aligned_1pxcross_postprocessed.png \
    --output-file examples/Fleur-de-lis_aligned_DMC_kmeans_scheme.png \
    --preferred-symbols 4,16     