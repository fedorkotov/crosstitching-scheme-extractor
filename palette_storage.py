#from selenium import webdriver
#from selenium.webdriver.common.keys import Keys

from sqlalchemy import create_engine, Column, ForeignKey, Integer, String, Binary, Boolean, UniqueConstraint
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker, load_only, aliased
from sqlalchemy import create_engine
import numpy as np
import logging

Base = declarative_base()



class MoulineManufacturer(Base):
    __tablename__ = 'MoulineManufacturers'
    id = Column(Integer, primary_key=True)
    name = Column(String(250), nullable=False, unique=True)
    colors=relationship("ThreadColor")
    
class ThreadColor(Base):
    __tablename__ = 'ThreadColors'
    id = Column(Integer, primary_key=True)
    manufacturer_id = Column(Integer, ForeignKey('MoulineManufacturers.id'))
    manufacturer = relationship("MoulineManufacturer", back_populates="colors")
    code = Column(String(10), nullable=False)
    red = Column(Integer, nullable=False)
    green = Column(Integer, nullable=False)
    blue = Column(Integer, nullable=False)
    L = Column(Integer, nullable=False)
    a = Column(Integer, nullable=False)
    b = Column(Integer, nullable=False)
    __table_args__ = (UniqueConstraint('manufacturer_id', 'code', name='idx_color_code_uniqueness'),)
    
    
class MoulinePaletteManager:    
    def __init__(self, db_path):
        self._db_path = db_path
        self._logger = logging.getLogger('MoulinePalleteManager')
        self._db_path=db_path
        self._engine = create_engine('sqlite:///'+db_path)        
        Base.metadata.create_all(self._engine)
        self._DBSession = sessionmaker(bind=self._engine)               
        
    def add_or_update_manufacturer(self, name):
        session = self._DBSession()
        manufacturer = \
            session\
            .query(MoulineManufacturer)\
            .filter(MoulineManufacturer.name==name)\
            .first()
        if(manufacturer==None):
            self._logger.debug('adding manufacturer "%s"', name)
            manufacturer = MoulineManufacturer(name=name)
            session.add(manufacturer)            
            session.commit()              
    
    def get_pallete(self, manufacturer_name):
        session = self._DBSession()
        
        
        manufacturer = \
            self._get_manufacturer_or_throw(\
                session,
                manufacturer_name)
        
        existing_colors_list =\
            session\
            .query(ThreadColor)\
            .filter(ThreadColor.manufacturer_id==manufacturer.id)\
            .order_by(ThreadColor.code)\
            .all()
        
        #manufacturer_alias = aliased(MoulineManufacturer)
        
        #existing_colors_list =\
        #    session\
        #    .query(ThreadColor)\
        #    .join(manufacturer_alias, ThreadColor.manufacturer)\
        #    .filter(manufacturer_alias.name==manufacturer_name)\
        #    .order_by(ThreadColor.code)\
        #    .all()
        
        bgrlab_array = np.empty((len(existing_colors_list), 6), dtype=np.uint8)
        names = []
        for idx,color in enumerate(existing_colors_list):
            names.append(color.code)
            bgrlab_array[idx,:]=\
                [color.blue,
                 color.green,
                 color.red,
                 color.L,
                 color.a,
                 color.b]
        return (names, bgrlab_array)
        
    def _update_color(self, manufacturer_name, color, bgrlab):
        color.red = int(bgrlab[2])
        color.green = int(bgrlab[1])
        color.blue = int(bgrlab[0])
        color.L = int(bgrlab[3])
        color.a = int(bgrlab[4])
        color.b = int(bgrlab[5])
        
        self._logger.debug(\
            "Updating color '%s' to rgb=(%s,%s,%s) for manufacturer id=%s",
            color.code,
            color.red,
            color.green,
            color.blue,
            manufacturer_name)
            
    def _create_color(self, 
                session, 
                manufacturer_id, 
                manufacturer_name, 
                code, 
                bgrlab):
        new_color = \
            ThreadColor(\
                manufacturer_id = manufacturer_id,
                code = code,
                red = int(bgrlab[2]),
                green = int(bgrlab[1]),
                blue = int(bgrlab[0]),
                L = int(bgrlab[3]),
                a = int(bgrlab[4]),
                b = int(bgrlab[5]))

        
        self._logger.debug(\
            "Adding color '%s' rgb=(%s,%s,%s) for manufacturer id=%s",
            new_color.code,
            new_color.red,
            new_color.green,
            new_color.blue,
            manufacturer_name)                    
        session.add(new_color)
    
    def _get_colors_dictionary(self, session, manufacturer_id, codes_list):
        existing_colors_list =\
            session\
            .query(ThreadColor)\
            .filter(ThreadColor.manufacturer_id==manufacturer_id)\
            .filter(ThreadColor.code.in_(codes_list))\
            .all()
            
        return dict([(x.code, x) for x in existing_colors_list])  
                
    def _get_manufacturer_or_throw(self, session, manufacturer_name):
        manufacturer = \
            session\
            .query(MoulineManufacturer)\
            .filter(MoulineManufacturer.name==manufacturer_name)\
            .first()
            
        if(manufacturer==None):
            raise ValueError("inexistent manufacturer '%s'" % manufacturer_name)
        
        return manufacturer 
    
        
    
    def add_or_update_colors(self, manufacturer_name, codes_list, bgrlab_array):
        session = self._DBSession()
        
        manufacturer = \
            self._get_manufacturer_or_throw(\
                session,
                manufacturer_name)
        
        existing_colors_dict = \
            self._get_colors_dictionary(\
                session,
                manufacturer.id,
                codes_list)
                
        for code, bgrlab in zip(codes_list, bgrlab_array):
            existing_color = existing_colors_dict.get(code, None)
            if(existing_color==None):
                self._create_color(\
                    session, 
                    manufacturer.id, 
                    manufacturer.name,
                    code, 
                    bgrlab)
            else:
                self._update_color(\
                    manufacturer.name,
                    existing_color, 
                    bgrlab)
        session.commit();
        
    
if __name__ == "__main__":
    logging.basicConfig(filename='pallete_storage.log', level=logging.DEBUG)

    pallete_storage = MoulinePaletteManager('test.sqlite')
    pallete_storage.add_or_update_manufacturer('DMC')
    pallete_storage.add_or_update_colors(
        'DMC',
        ['1','2'],
        np.array([[1,2,3,4,5,6],[7,8,9,10,11,12]], dtype=np.uint8))
        
    codes, colors =  pallete_storage.get_pallete('DMC')       
    
    print(codes)
    print(colors)
    
    
    

    
    
