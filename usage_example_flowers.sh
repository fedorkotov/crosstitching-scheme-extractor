#!/bin/bash

# Extracting row and column borders
echo ======================
./crosstiching_scheme.py \
    --action find-h-borders \
    --input-file examples/flowers_aligned_hgrid.png \
    --border-detector ave-min \
    --border-threshold 170 \
    --output-file examples/flowers_aligned_h.csv \
    --debug-output-img examples/flowers_aligned_h.png

echo ======================
./crosstiching_scheme.py \
    --action find-v-borders \
    --input-file examples/flowers_aligned_vgrid.png \
    --border-detector ave-min \
    --border-threshold 180 \
    --output-file examples/flowers_aligned_v.csv  \
    --debug-output-img examples/flowers_aligned_v.png

# Converting one pixel per cross image to mouline thread colors palette
echo ======================
./crosstiching_scheme.py \
    --action extract \
    --input-file examples/flowers_aligned.png \
    --h-boundaries examples/flowers_aligned_h.csv \
    --v-boundaries examples/flowers_aligned_v.csv \
    --output-file examples/flowers_aligned_1pxcross.png    


# Converting one pixel per cross image to mouline thread colors palette
echo ======================
./crosstiching_scheme.py \
    --action palette-kmeans-DMC \
    --ncolors 20 \
    --input-file examples/flowers_aligned_1pxcross_postprocessed.png \
    --output-file examples/flowers_aligned_DMC_kmeans_scheme.png \
    --preferred-symbols 1,2,3,4,16,18,21,22,24,25,33,34,39,54,58,59