[[_TOC_]]

# What is this script for?

This is a python script that solves two problems
1. extracting one pixel per cross images from photos or scans of crosstitching schemes
2. converting one pixel per cross images to schemes with automatic selection of nearest colors from mouline palettes of most popular embroidery supply manufacturers


# Usage examples

## Shades of gray Fleur-de-lis

### Extracting row and column borders

Script assumes that input images are not distorted (rows and columns are straight).
Before photo or scan can be used perspective distortions sould be removed with some 
raster images editor (e.g. GIMP)

`Fleur-de-lis_design.jpg` image in examples folder is taken from [here](
https://commons.wikimedia.org/wiki/File:Fleur-de-lis_design_(cross-stitch_pattern).jpg)
and is in public domain

![raw image](examples/Fleur-de-lis_design.jpg) 

**Image with perspective distortion removed**

![aligned image](examples/Fleur-de-lis_aligned.jpg)


Extracting row borders using `ave-min` border detector 
with threshold. `ave-min` places row boundaries at local minima of pixel value components (HSV color space) averaged over rows or columns.

```shell
./crosstiching_scheme.py \
    --action find-h-borders \
    --input-file examples/Fleur-de-lis_aligned.jpg \
    --border-detector ave-min \
    --border-threshold 110 \
    --output-file examples/Fleur-de-lis_aligned_h.csv \
    --debug-output-img examples/Fleur-de-lis_aligned_h.png

./crosstiching_scheme.py \
    --action find-v-borders \
    --input-file examples/Fleur-de-lis_aligned.jpg \
    --border-detector ave-min \
    --border-threshold 100 \
    --output-file examples/Fleur-de-lis_aligned_v.csv \
    --debug-output-img examples/Fleur-de-lis_aligned_v.png    
```

`Fleur-de-lis_aligned_h.png` and `Fleur-de-lis_aligned_v.png`

![row borders](examples/Fleur-de-lis_aligned_h.png) ![row borders](examples/Fleur-de-lis_aligned_v.png)

`Fleur-de-lis_aligned_h.csv` contents 
```
1,13,24,36,48,60,71,83,94,106,117,128,140,152,163,175,185,197,209,221,232,243,255,267,279,289,301,312,323,334,346,357,370
```

`Fleur-de-lis_aligned_v.csv` contents 
```
1,12,23,35,45,55,67,78,89,101,112,124,136,148,160,171,182,193,205,217,228,240,252,263,274,286,298,310,321,332
```

### Extracting one pixel per cross image

Extracting one pixel per cross image using pre-calculated row and column borders. Pixel colors are calculated as average cell colors.
```shell
./crosstiching_scheme.py \
    --action extract \
    --input-file examples/Fleur-de-lis_aligned.jpg \
    --h-boundaries examples/Fleur-de-lis_aligned_h.csv \
    --v-boundaries examples/Fleur-de-lis_aligned_v.csv \
    --output-file examples/Fleur-de-lis_aligned_1pxcross.png    
```

`Fleur-de-lis_aligned_1pxcross.png` 

![1 pixel per cross image](examples/Fleur-de-lis_aligned_1pxcross_large.png)

Resulting image often requires postprocessing before crossstitching scheme can be produced.

`Fleur-de-lis_aligned_1pxcross_postprocessed.png` - result of applying curves and threshold filters in GIMP with manual removal of two one-pixel artifactis:

![postprocessed image](examples/Fleur-de-lis_aligned_1pxcross_postprocessed_large.png)

### Converting one pixel per cross image to mouline thread colors palette

Converting one pixel per cross image to [DMC](https://www.dmc.com/) embroidery supplies manufacturer mouline colors palette using [k-means clustering](https://docs.scipy.org/doc/scipy/reference/generated/scipy.cluster.vq.kmeans.html) to reduce number of colors to 2. Clustering is not required for black and white example image but I could not find sutable color image with permissive enough license to use it here.

```shell
./crosstiching_scheme.py \
    --action palette-kmeans-DMC \
    --ncolors 2 \
    --input-file examples/Fleur-de-lis_aligned_1pxcross_postprocessed.png \
    --output-file examples/Fleur-de-lis_aligned_DMC_kmeans_scheme.png \
    --preferred-symbols 4,16   
```    

`Fleur-de-lis_aligned_DMC_kmeans_scheme.png_scheme.png`

![scheme](examples/Fleur-de-lis_aligned_DMC_kmeans_scheme.png_scheme.png)

[DMC threads palette](examples/Fleur-de-lis_aligned_DMC_kmeans_scheme.png_palette.htm)


## Coloured scheme (flowers)

### Extracting row and column borders

`flowers.jpg` image in examples folder is taken from [here](http://patternmakercharts.blogspot.com/2016/06/click-here.html)

![raw image](examples/flowers.jpg) 

**Image with perspective distortion removed**

![aligned image](examples/flowers_aligned.png)

Cell borders on this image are obstructed by hand-painted colours.
I had to cut vertical (`flowers_aligned_hgrid.png`) and horizontal (`flowers_aligned_vgrid.png`) bands with GIMP before
automatic cell borders extraction with the script.

```shell
./crosstiching_scheme.py \
    --action find-h-borders \
    --input-file examples/flowers_aligned_hgrid.png \
    --border-detector ave-min \
    --border-threshold 170 \
    --output-file examples/flowers_aligned_h.csv \
    --debug-output-img examples/flowers_aligned_h.png

./crosstiching_scheme.py \
    --action find-v-borders \
    --input-file examples/flowers_aligned_vgrid.png \
    --border-detector ave-min \
    --border-threshold 180 \
    --output-file examples/flowers_aligned_v.csv  \
    --debug-output-img examples/flowers_aligned_v.png  
```  

![horizontal band](examples/flowers_aligned_v.png)
![vertical band](examples/flowers_aligned_h.png)

### Extracting one pixel per cross image

```shell
./crosstiching_scheme.py \
    --action extract \
    --input-file examples/flowers_aligned.png \
    --h-boundaries examples/flowers_aligned_h.csv \
    --v-boundaries examples/flowers_aligned_v.csv \
    --output-file examples/flowers_aligned_1pxcross.png   
```

`flowers_aligned_1pxcross.png` 

![1 pixel per cross image](examples/flowers_aligned_1pxcross_large.png)

Manual postprocessing result
`flowers_aligned_1pxcross_postprocessed.png` 

![1 pixel per cross image](examples/flowers_aligned_1pxcross_postprocessed_large.png)

### Converting one pixel per cross image to mouline thread colors palette

```shell
./crosstiching_scheme.py \
    --action palette-kmeans-DMC \
    --ncolors 20 \
    --input-file examples/flowers_aligned_1pxcross_postprocessed.png \
    --output-file examples/flowers_aligned_DMC_kmeans_scheme.png \
    --preferred-symbols 1,2,3,4,16,18,21,22,24,25,33,34,39,54,58,59 
```    

`Fleur-de-lis_aligned_DMC_kmeans_scheme.png_scheme.png`

![scheme](examples/flowers_aligned_DMC_kmeans_scheme.png_scheme.png)

[DMC threads palette](examples/flowers_aligned_DMC_kmeans_scheme.png_palette.htm)
